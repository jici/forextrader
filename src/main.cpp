#include <include/ProcessManager.h>

int main() {

	ProcessManager processManager(CONFIGPATH);
	processManager.StartSystem();

	return 0;
}